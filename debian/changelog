libclass-dbi-plugin-pager-perl (0.566-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 01:45:03 +0100

libclass-dbi-plugin-pager-perl (0.566-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.
  * Remove Tim Retout from Uploaders. Thanks for your work!

  [ Damyan Ivanov ]
  * change Priority from 'extra' to 'optional'

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org

  [ gregor herrmann ]
  * Remove Ben Hutchings from Uploaders on his request. (Closes: #950285)
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on libmodule-build-perl.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 11 Jun 2022 22:27:12 +0100

libclass-dbi-plugin-pager-perl (0.566-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Thu, 07 Jan 2021 17:17:05 +0100

libclass-dbi-plugin-pager-perl (0.566-2) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: update Module::Build dependency.
  * Add debian/upstream/metadata
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.6.
  * Mark package as autopkgtest-able.

 -- gregor herrmann <gregoa@debian.org>  Mon, 01 Jun 2015 20:47:55 +0200

libclass-dbi-plugin-pager-perl (0.566-1) unstable; urgency=low

  [ Tim Retout ]
  * Email change: Tim Retout -> diocles@debian.org

  [ Ansgar Burchardt ]
  * Email change: Ansgar Burchardt -> ansgar@debian.org
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Xavier Guimard ]
  * Imported Upstream version 0.566
  * Remove fix-tests-new-sql-abstract.patch included now in upstream
  * Use debhelper 8
  * Bump Standards-Version to 3.9.4
  * Add libuniversal-require-perl in dependencies
  * Update debian/copyright (years and format)
  * Add "libmodule-build-perl (>= 0.400000) | perl (>= 5.17.1)" in
    Build-Depends
  * Clean description

 -- Xavier Guimard <x.guimard@free.fr>  Sun, 02 Dec 2012 16:54:38 +0100

libclass-dbi-plugin-pager-perl (0.561-4) unstable; urgency=low

  * Update patch to use correct SQL comparison operator. (Closes: #576644)
  * debian/control: Make dependency on libsql-abstract-perl versioned, same as
    the Build-Depends.
  * Use source format 3.0 (quilt); drop quilt framework and README.source.
  * debian/copyright: Minor formatting changes for current DEP-5 proposal.
  * Bump Standards-Version to 3.8.4 (no changes).
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@43-1.org>  Wed, 07 Apr 2010 22:46:14 +0900

libclass-dbi-plugin-pager-perl (0.561-3) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Tim Retout ]
  * Switch to 3-line rules file with quilt.
    + Update debhelper compat and Build-Dependency to 7.
    + Add quilt Build-Dependency.
  * Add patch to fix 02main.t with libsql-abstract-perl (>= 1.55), and
    Build-Depend on that. (Closes: #531230)
  * Set urgency to medium for RC bug fix.
  * Add self to Uploaders.
  * Update Standards-Version to 3.8.2.
    + Add standard quilt README.source.

  [ gregor herrmann ]
  * debian/control: drop build dependency on libmodule-build-perl, debhelper
    uses EUMM.
  * debian/copyright: update format and info about debian/*.

 -- Tim Retout <tim@retout.co.uk>  Wed, 05 Aug 2009 00:24:41 +0100

libclass-dbi-plugin-pager-perl (0.561-2) unstable; urgency=low

  * Taking over maintenance with Ben's approval
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Changed:
    Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org> (was: Ben Hutchings
    <ben@decadentplace.org.uk>); Ben Hutchings
    <ben@decadentplace.org.uk> moved to Uploaders.
  * Add debian/watch.
  * debian/rules: remade with dh-make-perl template
    + stop installing README as it contains nothing more that the POD
  * move debhelper, perl and libmodule-build-perl from B-D-I to B-D.
  * bump debhelper compatibility level to 5
  * Standards-Version 3.7.3 (no changes)
  * add myself to Uploaders
  * remove Homepage pseudo-field from long description
  * debian/copyright: use dist-based upstream URL
    + convert to machine-readable format

 -- Damyan Ivanov <dmn@debian.org>  Sun, 10 Feb 2008 00:08:21 +0200

libclass-dbi-plugin-pager-perl (0.561-1) unstable; urgency=low

  * Initial release - closes: #345453

 -- Ben Hutchings <ben@decadentplace.org.uk>  Sat, 31 Dec 2005 22:45:23 +0000
